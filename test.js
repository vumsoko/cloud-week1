//first NodeJS application: This is a simple application that will create a restful webservice listening on port 3000.
const express = require('express')
const app = express()
const port = 3000
app.get('/', (req, res) => {
res.send('Hello World!')
})
app.listen(port, () => {
console.log(`Express Application listening at port 3000`)
})
